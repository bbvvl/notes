package vvl.notes.recyclerView

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.annotation.TargetApi
import android.os.Build
import android.view.View
import vvl.notes.util.DEFAULT_ANIMATION_DURATION
import vvl.notes.util.DEFAULT_Z_VALUE
import android.opengl.ETC1.getHeight
import android.opengl.ETC1.getHeight
import android.opengl.ETC1.getWidth


class MyItemTouchHelperCallback(private val adapter: ItemTouchHelperAdapter) : ItemTouchHelper.Callback() {

    override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
        val dragFlags = ItemTouchHelper.UP or ItemTouchHelper.DOWN
        val swipeFlags = ItemTouchHelper.START or ItemTouchHelper.END
        return makeMovementFlags(dragFlags, swipeFlags)
    }

    override fun onMove(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        return viewHolder.itemViewType == target.itemViewType && adapter.onMove(viewHolder.adapterPosition, target.adapterPosition)
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder?, direction: Int) {
        adapter.onItemDismiss(viewHolder!!.adapterPosition)
    }

    override fun isLongPressDragEnabled(): Boolean = true

    override fun isItemViewSwipeEnabled(): Boolean = true

    override fun onChildDraw(c: Canvas?, recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {

        val itemView = viewHolder!!.itemView
        when (actionState) {
            ItemTouchHelper.ACTION_STATE_SWIPE -> {
                //TODO bitmap
                when {
                    (dX > 0) -> {
                        //c.drawBitmap(rightIcon, view.getRight() - rightIcon.getWidth() - iconMarginPx, view.getBottom() + view.getTop() - rightIcon.getHeight() shr 1, iconPaint)

                        c!!.drawRect(itemView.left.toFloat() - recyclerView!!.paddingLeft, itemView.top.toFloat(), itemView.right.toFloat() / 2 + dX,
                                itemView.bottom.toFloat(), Paint().apply { color = Color.RED })
                    }
                    (dX < 0) -> {
                        //c.drawBitmap(leftIcon, view.getLeft() + iconMarginPx, view.getBottom() + view.getTop() - leftIcon.getHeight() shr 1, iconPaint)
                        c!!.drawRect(itemView.right.toFloat() / 2 + dX, itemView.top.toFloat(),
                                itemView.right.toFloat() + recyclerView!!.paddingRight, itemView.bottom.toFloat(), Paint().apply { color = Color.GREEN })
                    }
                    else -> {
                        c!!.drawRect(itemView.left.toFloat(), itemView.top.toFloat(),
                                itemView.right.toFloat(), itemView.bottom.toFloat(), Paint().apply { color = Color.WHITE })
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            }
            ItemTouchHelper.ACTION_STATE_DRAG -> {
                val newY = when {
                    recyclerView!!.top > (itemView.top + dY) -> (recyclerView.top - itemView.top).toFloat()
                    recyclerView.bottom < (itemView.bottom + dY) -> (recyclerView.bottom - itemView.bottom).toFloat()
                    else -> dY
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, newY, actionState, isCurrentlyActive)
            }
        }
    }

    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
        if (actionState == ItemTouchHelper.ACTION_STATE_DRAG) {
            changeElevation(viewHolder!!.itemView, DEFAULT_Z_VALUE, DEFAULT_ANIMATION_DURATION)
        }
        super.onSelectedChanged(viewHolder, actionState)
    }

    override fun clearView(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?) {
        super.clearView(recyclerView, viewHolder)
        changeElevation(viewHolder!!.itemView, 0f, DEFAULT_ANIMATION_DURATION)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private fun changeElevation(target: View, toDp: Float, durationMillis: Long) {
        target.animate().translationZ(toDp).setDuration(durationMillis).start()
    }
}