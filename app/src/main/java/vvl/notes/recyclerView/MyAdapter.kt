package vvl.notes.recyclerView

import android.support.v7.util.DiffUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import io.realm.OrderedRealmCollection
import io.realm.Realm
import io.realm.RealmRecyclerViewAdapter
import io.realm.Sort
import io.realm.kotlin.where
import vvl.notes.R
import vvl.notes.db.DataHelper
import vvl.notes.db.Note


class MyAdapter(private val callback: MyCallback, private var notes: OrderedRealmCollection<Note>) : RealmRecyclerViewAdapter<Note, MyViewHolder>(notes, true), ItemTouchHelperAdapter {

    init {
        setHasStableIds(true)
    }

    override fun onMoved(fromPosition: Int, toPosition: Int) {
        //callback.move(getItemId(fromPosition), getItemId(toPosition), fromPosition, toPosition)
    }

    override fun onMove(fromPosition: Int, toPosition: Int): Boolean {

        callback.move(getItemId(fromPosition), getItemId(toPosition))
        return true
    }

    override fun onItemDismiss(position: Int) {
        callback.removeAt(getItemId(position))
    }

    override fun onBindViewHolder(holder: MyViewHolder?, position: Int) {
        holder!!.onBind(getItem(position)!!,callback)


//        mItemTouchHelper.startDrag(viewHolder);

    }

    fun swap(realm: Realm, sortType: DataHelper.SortType) {
        val actors: OrderedRealmCollection<Note>
        val diffCallback: DiffUtil.Callback = when (sortType) {
            DataHelper.SortType.Time -> {
                actors = realm.where<Note>().sort("time").findAll()
                ActorDiffCallback(notes, actors)
            }
            DataHelper.SortType.Note -> {
                actors = realm.where<Note>().sort("position",Sort.DESCENDING).findAll()
                ActorDiffCallback(notes, actors)
            }
            DataHelper.SortType.Position -> {
                actors = realm.where<Note>().sort("position").findAll()
                ActorDiffCallback(notes, actors)
            }
        }
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        notes = actors
        //notes.addAll(actors)
        diffResult.dispatchUpdatesTo(this)

    }

    override fun getItemId(index: Int): Long {
        return getItem(index)!!.id
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.recycler_view_item, parent, false))
    }
}