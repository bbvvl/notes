package vvl.notes.recyclerView

import io.realm.Realm

interface MyCallback {

    fun removeAt(position: Long)
    fun move(fromId: Long, toId: Long)
    fun onTextChanged(changed:String,changedId: Long)
}