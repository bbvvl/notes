package vvl.notes.recyclerView

import android.support.v7.widget.RecyclerView


interface ItemTouchHelperAdapter {


    fun onMove(fromPosition: Int, toPosition: Int): Boolean

    fun onMoved(fromPosition: Int, toPosition: Int)

    fun onItemDismiss(position: Int)

}
