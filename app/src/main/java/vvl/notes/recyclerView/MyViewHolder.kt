package vvl.notes.recyclerView

import android.os.Handler
import android.support.v4.view.MotionEventCompat
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.MotionEvent
import android.view.View
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import vvl.notes.R
import vvl.notes.db.Note
import vvl.notes.util.packageName


class MyViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

/*
    private val handler = Handler()
    private var updateCallback = Runnable {}
*/
    private val et: EditText = view.findViewById(R.id.recycler_view_item_edit_text)
    private val cv: CardView = view.findViewById(R.id.recycler_view_item_card_view)
    private val ib: ImageButton = view.findViewById(R.id.recycler_view_item_image_button)

    fun onBind(note: Note, callback: MyCallback) {
        et.setText(note.note)
        var color = note.color
        color = view.resources.getColor(view.resources.getIdentifier("background" + (color + 1), "color", packageName))
        cv.setCardBackgroundColor(color)
        // TODO text...

/*        et.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                handler.removeCallbacks(updateCallback)
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                updateCallback = Runnable {
                    callback.onTextChanged(s.toString(), note.id)
                }
                handler.postDelayed(updateCallback, 500)

            }
        })*/
    }
}