package vvl.notes.fab.animation

import android.animation.Animator
import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.graphics.Point
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.OvershootInterpolator
import vvl.notes.fab.FloatingActionMenu


/**
 * @see https://github.com/oguzbilgener/CircularFloatingActionMenu
 *
 */
class DefaultAnimationHandler : MenuAnimationHandler() {

    /** duration of animations, in milliseconds  */
    private val DURATION = 500L
    /** duration to wait between each of   */
    private val LAG_BETWEEN_ITEMS = 20L

    /** holds the current state of animation  */
    override var isAnimating: Boolean = false

    private val animations: MutableList<Animator> = ArrayList()

    override fun cancelAnimations() {
        super.cancelAnimations()
        for (i in 0 until animations.size)
            animations[i].cancel()
    }

    private fun startAnimations() {
        for (i in 0 until animations.size)
            animations[i].start()
    }

    override fun animateMenuOpening(center: Point) {
        super.animateMenuOpening(center)

        isAnimating = true
        animations.clear()
        for (i in 0 until menu!!.subActionItems.size) {

            menu!!.subActionItems[i].view.scaleX = 0f
            menu!!.subActionItems[i].view.scaleY = 0f
            menu!!.subActionItems[i].view.alpha = 0f

            val pvhX = PropertyValuesHolder.ofFloat(View.TRANSLATION_X, (menu!!.subActionItems[i].x - center.x + menu!!.subActionItems[i].width / 2).toFloat())
            val pvhY = PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, (menu!!.subActionItems[i].y - center.y + menu!!.subActionItems[i].height / 2).toFloat())
            val pvhR = PropertyValuesHolder.ofFloat(View.ROTATION, 720f)
            val pvhsX = PropertyValuesHolder.ofFloat(View.SCALE_X, 1f)
            val pvhsY = PropertyValuesHolder.ofFloat(View.SCALE_Y, 1f)
            val pvhA = PropertyValuesHolder.ofFloat(View.ALPHA, 1f)

            val animation = ObjectAnimator.ofPropertyValuesHolder(menu!!.subActionItems[i].view, pvhX, pvhY, pvhR, pvhsX, pvhsY, pvhA)
            animation.duration = DURATION
            animation.interpolator = OvershootInterpolator(0.9f)
            animation.addListener(SubActionItemAnimationListener(menu!!.subActionItems[i], MenuAnimationHandler.ActionType.OPENING))

            animations.add(animation.apply { addListener(LastAnimationListener()) })
            // Put a slight lag between each of the menu items to make it asymmetric
            //animation.startDelay = (menu!!.subActionItems.size - i) * LAG_BETWEEN_ITEMS
            //animation.start()
        }

        startAnimations()
    }

    override fun animateMenuClosing(center: Point) {
        super.animateMenuOpening(center)

        isAnimating = true
        animations.clear()
        for (i in 0 until menu!!.subActionItems.size) {
            val pvhX = PropertyValuesHolder.ofFloat(View.TRANSLATION_X, -(menu!!.subActionItems[i].x - center.x + menu!!.subActionItems[i].width / 2).toFloat())
            val pvhY = PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, -(menu!!.subActionItems[i].y - center.y + menu!!.subActionItems[i].height / 2).toFloat())
            val pvhR = PropertyValuesHolder.ofFloat(View.ROTATION, -720f)
            val pvhsX = PropertyValuesHolder.ofFloat(View.SCALE_X, 0f)
            val pvhsY = PropertyValuesHolder.ofFloat(View.SCALE_Y, 0f)
            val pvhA = PropertyValuesHolder.ofFloat(View.ALPHA, 0f)

            val animation = ObjectAnimator.ofPropertyValuesHolder(menu!!.subActionItems[i].view, pvhX, pvhY, pvhR, pvhsX, pvhsY, pvhA)
            animation.duration = DURATION
            animation.interpolator = AccelerateDecelerateInterpolator()
            animation.addListener(SubActionItemAnimationListener(menu!!.subActionItems[i], MenuAnimationHandler.ActionType.CLOSING))

            animations.add(animation.apply { addListener(LastAnimationListener()) })

        }
        startAnimations()
    }

    private inner class SubActionItemAnimationListener(private val subActionItem: FloatingActionMenu.Item, private val actionType: MenuAnimationHandler.ActionType) : Animator.AnimatorListener {

        override fun onAnimationStart(animation: Animator) {
        }

        override fun onAnimationEnd(animation: Animator) {
            restoreSubActionViewAfterAnimation(subActionItem, actionType)
        }

        override fun onAnimationCancel(animation: Animator) {
            restoreSubActionViewAfterAnimation(subActionItem, actionType)
        }

        override fun onAnimationRepeat(animation: Animator) {}
    }
}