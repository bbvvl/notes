package vvl.notes.fab.animation

import android.animation.Animator
import android.graphics.Point
import android.view.WindowManager
import android.widget.FrameLayout
import vvl.notes.fab.FloatingActionMenu


/**
 * @see github.com/oguzbilgener/CircularFloatingActionMenu
 * */
abstract class MenuAnimationHandler {

    var menu: FloatingActionMenu? = null

    abstract var isAnimating: Boolean

    protected enum class ActionType {
        OPENING, CLOSING
    }

    /**
     * Starts the opening animation
     * Should be overriden by children
     * @param center
     */
    open fun animateMenuOpening(center: Point) {
        if (menu == null) {
            throw NullPointerException("MenuAnimationHandler cannot animate without a valid FloatingActionMenu.")
        }

    }

    /**
     * Ends the opening animation
     * Should be overriden by children
     * @param center
     */
    open fun animateMenuClosing(center: Point) {
        if (menu == null) {
            throw NullPointerException("MenuAnimationHandler cannot animate without a valid FloatingActionMenu.")
        }
    }

    open fun cancelAnimations() {
        if (menu == null) {
            throw NullPointerException("MenuAnimationHandler cannot animate without a valid FloatingActionMenu.")
        }
        if (!isAnimating) return
        isAnimating = false
    }

    /**
     * Restores the specified sub action view to its final state, according to the current actionType
     * Should be called after an animation finishes.
     * @param subActionItem
     * @param actionType
     */
    protected fun restoreSubActionViewAfterAnimation(subActionItem: FloatingActionMenu.Item, actionType: ActionType) {
        val params = subActionItem.view.layoutParams
        subActionItem.view.translationX = 0f
        subActionItem.view.translationY = 0f
        subActionItem.view.rotation = 0f
        subActionItem.view.scaleX = 1f
        subActionItem.view.scaleY = 1f
        subActionItem.view.alpha = 1f

        when (actionType) {
            ActionType.OPENING -> {
                val lp = params as FrameLayout.LayoutParams
                if (menu!!.systemOverlay) {
                    val overlayParams = menu!!.overlayContainer!!.layoutParams as WindowManager.LayoutParams
                    lp.setMargins(subActionItem.x - overlayParams.x, subActionItem.y - overlayParams.y, 0, 0)
                } else {
                    lp.setMargins(subActionItem.x, subActionItem.y, 0, 0)
                }
                subActionItem.view.layoutParams = lp
            }
            ActionType.CLOSING -> {
                val center = menu!!.getActionViewCenter()
                val lp = params as FrameLayout.LayoutParams
                if (menu!!.systemOverlay) {
                    val overlayParams = menu!!.overlayContainer!!.layoutParams as WindowManager.LayoutParams
                    lp.setMargins(center.x - overlayParams.x - subActionItem.width / 2, center.y - overlayParams.y - subActionItem.height / 2, 0, 0)
                } else {
                    lp.setMargins(center.x - subActionItem.width / 2, center.y - subActionItem.height / 2, 0, 0)
                }
                subActionItem.view.layoutParams = lp
                menu!!.removeViewFromCurrentContainer(subActionItem.view)

                if (menu!!.systemOverlay) {
                    // When all the views are removed from the overlay container,
                    // we also need to detach it
                    if (menu!!.overlayContainer!!.childCount == 0) {
                        menu!!.detachOverlayContainer()
                    }
                }
            }
        }
    }

    /**
     * A special animation listener that is intended to listen the last of the sequential animations.
     * Changes the animating property of children.
     */
    open inner class LastAnimationListener : Animator.AnimatorListener {

        override fun onAnimationStart(animation: Animator) {
            isAnimating = true
        }

        override fun onAnimationEnd(animation: Animator) {
            isAnimating = false
        }

        override fun onAnimationCancel(animation: Animator) {
            isAnimating = false
        }

        override fun onAnimationRepeat(animation: Animator) {
            isAnimating = true
        }
    }
}

