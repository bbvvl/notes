package vvl.notes.fab

import android.app.Activity
import android.content.Context
import android.graphics.*
import android.hardware.SensorManager
import android.os.Build
import android.view.*
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import vvl.notes.R
import vvl.notes.fab.animation.DefaultAnimationHandler
import vvl.notes.fab.animation.MenuAnimationHandler

/**
 * @see https://github.com/oguzbilgener/CircularFloatingActionMenu
 * */
class FloatingActionMenu(
        /** Reference to the view (usually a button) to trigger the menu to show */
        private val mainActionView: View,
        /** The angle (in degrees, modulus 360) which the circular menu starts from  */
        private val startAngle: Int,
        /** The angle (in degrees, modulus 360) which the circular menu ends at  */
        private val endAngle: Int,
        /** Distance of menu items from mainActionView */
        private val radius: Int,
        /** List of menu items */
        val subActionItems: List<Item>, animationHandler: MenuAnimationHandler, animated: Boolean, stateChangeListener: MenuStateChangeListener?, private val onClickListener: OnClickListener?, systemOverlay: Boolean) {

    /** Reference to the preferred {@link MenuAnimationHandler} object */
    private val animationHandler: MenuAnimationHandler?
    /** Reference to a listener that listens open/close actions */
    var stateChangeListener: MenuStateChangeListener?
    /** whether the openings and closings should be animated or not */
    private val animated: Boolean
    /** whether the menu is currently open or not */
    private var open: Boolean
    /** whether the menu is an overlay for all other activities */
    val systemOverlay: Boolean
    /** a simple layout to contain all the sub action views in the system overlay mode */
    val overlayContainer: FrameLayout?

    private lateinit var orientationListener: OrientationEventListener

    /**
     * Simply opens the menu by doing necessary calculations.
     * @param animated if true, this action is executed by the current {@link MenuAnimationHandler}
     */
    private fun open(animated: Boolean) {

        // Get the center of the action view from the following function for efficiency
        // populate destination x,y coordinates of Items
        val center = calculateItemPositions()

        var overlayParams: WindowManager.LayoutParams? = null

        if (systemOverlay) {
            // If this is a system overlay menu, use the overlay container and place it behind
            // the main action button so that all the views will be added into it.
            attachOverlayContainer()

            overlayParams = overlayContainer!!.layoutParams as (WindowManager.LayoutParams)
        }

        if (animated && animationHandler != null) {
            // If animations are enabled and we have a MenuAnimationHandler, let it do the heavy work
            if (animationHandler.isAnimating) animationHandler.cancelAnimations()


            for (i in 0 until subActionItems.size) {
                // It is required that these Item views are not currently added to any parent
                // Because they are supposed to be added to the Activity content view,
                // just before the animation starts
                if (subActionItems[i].view.parent != null) {
                    throw  RuntimeException("All of the sub action items have to be independent from a parent.")
                }

                // Initially, place all items right at the center of the main action view
                // Because they are supposed to start animating from that point.
                val params = FrameLayout.LayoutParams(subActionItems[i].width, subActionItems[i].height, Gravity.TOP or Gravity.LEFT)

                if (systemOverlay) {
                    params.setMargins(center.x - overlayParams!!.x - subActionItems[i].width / 2, center.y - overlayParams.y - subActionItems.get(i).height / 2, 0, 0)
                } else {
                    params.setMargins(center.x - subActionItems[i].width / 2, center.y - subActionItems[i].height / 2, 0, 0)
                }
                addViewToCurrentContainer(subActionItems[i].view, params)
            }
            // Tell the current MenuAnimationHandler to animate from the center
            animationHandler.animateMenuOpening(center)
        } else {
            // If animations are disabled, just place each of the items to their calculated destination positions.
            for (i in 0 until subActionItems.size) {
                // This is currently done by giving them large margins

                val params = FrameLayout.LayoutParams(subActionItems[i].width, subActionItems[i].height, Gravity.TOP or Gravity.LEFT)
                if (systemOverlay) {
                    params.setMargins(subActionItems[i].x - overlayParams!!.x, subActionItems[i].y - overlayParams.y, 0, 0)
                    subActionItems[i].view.layoutParams = params
                } else {
                    params.setMargins(subActionItems[i].x, subActionItems[i].y, 0, 0)
                    subActionItems[i].view.layoutParams = params
                    // Because they are placed into the main content view of the Activity,
                    // which is itself a FrameLayout
                }
                addViewToCurrentContainer(subActionItems[i].view, params)
            }
        }
        // do not forget to specify that the menu is open.
        open = true

        for (i in 0 until subActionItems.size) {
            subActionItems[i].view.setOnClickListener { toggle(true); onClickListener!!.onClick(i) }
        }

        if (stateChangeListener != null) {
            stateChangeListener!!.onMenuOpened(this)
        }

    }

    /**
     * Closes the menu.
     * @param animated if true, this action is executed by the current {@link MenuAnimationHandler}
     */
    private fun close(animated: Boolean) {
        // If animations are enabled and we have a MenuAnimationHandler, let it do the heavy work
        if (animated && animationHandler != null) {
            if (animationHandler.isAnimating) animationHandler.cancelAnimations()

            animationHandler.animateMenuClosing(getActionViewCenter())
        } else {
            // If animations are disabled, just detach each of the Item views from the Activity content view.
            for (i in 0 until subActionItems.size) {
                removeViewFromCurrentContainer(subActionItems.get(i).view)
            }
            if (systemOverlay)
                detachOverlayContainer()
        }
        // do not forget to specify that the menu is now closed.
        open = false

        for (i in 0 until subActionItems.size) {
            subActionItems[i].view.setOnClickListener(null)
        }

        if (stateChangeListener != null) {
            stateChangeListener!!.onMenuClosed(this)
        }
    }

    /**
     * Toggles the menu
     * @param animated if true, the open/close action is executed by the current {@link MenuAnimationHandler}
     */
    fun toggle(animated: Boolean) {
        if (open) {
            mainActionView.startAnimation(AnimationUtils.loadAnimation(mainActionView.context, R.anim.rotate_backrward))
            close(animated)
        } else {
            mainActionView.startAnimation(AnimationUtils.loadAnimation(mainActionView.context, R.anim.rotate_forward))
            open(animated)
        }
    }


    /**
     * Recalculates the positions of each sub action item on demand.
     */
    public fun updateItemPositions() {
        // Only update if the menu is currently open
        if (!open) {
            return
        }
        // recalculate x,y coordinates of Items
        calculateItemPositions()

        // Simply update layout params for each item
        for (i in 0 until subActionItems.size) {
            // This is currently done by giving them large margins
            val params = FrameLayout.LayoutParams(subActionItems[i].width, subActionItems[i].height, Gravity.TOP or Gravity.LEFT)
            params.setMargins(subActionItems.get(i).x, subActionItems[i].y, 0, 0)
            subActionItems[i].view.layoutParams = params
        }
    }

    /**
     * Gets the coordinates of the main action view
     * This method should only be called after the main layout of the Activity is drawn,
     * such as when a user clicks the action button.
     * @return a Point containing x and y coordinates of the top left corner of action view
     */
    private fun getActionViewCoordinates(): Point {
        val coordinates = IntArray(2)
        // This method returns a x and y values that can be larger than the dimensions of the device screen.
        mainActionView.getLocationOnScreen(coordinates)

        // So, we need to deduce the offsets.
        if (systemOverlay) {
            coordinates[1] -= getStatusBarHeight()
        } else {
            val activityFrame = Rect()
            getActivityContentView().getWindowVisibleDisplayFrame(activityFrame)
            coordinates[0] -= (getScreenSize().x - getActivityContentView().measuredWidth)
            coordinates[1] -= (activityFrame.height() + activityFrame.top - getActivityContentView().measuredHeight)
        }
        return Point(coordinates[0], coordinates[1])
    }


    fun getActionViewCenter(): Point {
        val point = getActionViewCoordinates()
        point.x += mainActionView.measuredWidth / 2
        point.y += mainActionView.measuredHeight / 2
        return point
    }

    /**
     * Calculates the desired positions of all items.
     * @return getActionViewCenter()
     */
    private fun calculateItemPositions(): Point {
        // Create an arc that starts from startAngle and ends at endAngle
        // in an area that is as large as 4*radius^2
        val center = getActionViewCenter()
        val area = RectF((center.x - radius).toFloat(), (center.y - radius).toFloat(), (center.x + radius).toFloat(), (center.y + radius).toFloat())

        val orbit = Path()
        orbit.addArc(area, startAngle.toFloat(), (endAngle - startAngle).toFloat())

        val measure = PathMeasure(orbit, false)


        val divisor =
                if (Math.abs(endAngle - startAngle) >= 360 || subActionItems.size <= 1)
                    subActionItems.size
                else
                    subActionItems.size - 1


        // Measure this path, in order to find points that have the same distance between each other
        for (i in 0 until subActionItems.size) {
            val coordinates = FloatArray(2) { 0f }

            measure.getPosTan((i) * measure.length / divisor, coordinates, null)
            // get the x and y values of these points and set them to each of sub action items.
            subActionItems[i].x = (coordinates[0] - subActionItems[i].width / 2).toInt()
            subActionItems[i].y = (coordinates[1] - subActionItems[i].height / 2).toInt()
        }
        return center
    }

    private fun getActivityContentView(): View {
        try {
            return ((mainActionView.context) as Activity).window.decorView.findViewById(android.R.id.content)
        } catch (e: ClassCastException) {
            throw  ClassCastException("Please provide an Activity context for this FloatingActionMenu.")
        }
    }

    private fun getWindowManager(): WindowManager {
        return mainActionView.context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
    }

    private fun addViewToCurrentContainer(view: View, layoutParams: ViewGroup.LayoutParams? = null) {
        if (systemOverlay) {
            overlayContainer!!.addView(view, layoutParams)
        } else {
            try {
                if (layoutParams != null) {
                    val lp = layoutParams as (FrameLayout.LayoutParams)
                    (getActivityContentView() as (ViewGroup)).addView(view, lp)
                } else {
                    (getActivityContentView() as (ViewGroup)).addView(view)
                }
            } catch (e: ClassCastException) {
                throw ClassCastException("layoutParams must be an instance of " +
                        "FrameLayout.LayoutParams.")
            }
        }
    }

    fun attachOverlayContainer() {
        try {
            val overlayParams = calculateOverlayContainerParams()

            overlayContainer!!.layoutParams = overlayParams
            if (overlayContainer.parent == null) {
                getWindowManager().addView(overlayContainer, overlayParams)
            }
            getWindowManager().updateViewLayout(mainActionView, mainActionView.getLayoutParams())
        } catch (e: SecurityException) {
            throw SecurityException("Your application must have SYSTEM_ALERT_WINDOW " +
                    "permission to create a system window.")
        }
    }

    private fun calculateOverlayContainerParams(): WindowManager.LayoutParams {

        val overlayParams = getDefaultSystemWindowParams()
        var left = 9999
        var right = 0
        var top = 9999
        var bottom = 0
        for (i in 0 until subActionItems.size) {
            val lm = subActionItems[i].x
            val tm = subActionItems[i].y
            if (lm < left) {
                left = lm
            }
            if (tm < top) {
                top = tm
            }
            if (lm + subActionItems[i].width > right) {
                right = lm + subActionItems[i].width
            }
            if (tm + subActionItems[i].height > bottom) {
                bottom = tm + subActionItems[i].height
            }
        }
        overlayParams.width = right - left
        overlayParams.height = bottom - top
        overlayParams.x = left
        overlayParams.y = top
        overlayParams.gravity = Gravity.TOP or Gravity.LEFT
        return overlayParams
    }

    fun detachOverlayContainer() {
        println(overlayContainer)
        getWindowManager().removeView(overlayContainer)
    }

    private fun getStatusBarHeight(): Int {

        val resourceId = mainActionView.context.resources.getIdentifier("status_bar_height", "dimen", "android")
        return if (resourceId > 0) {
            mainActionView.context.resources.getDimensionPixelSize(resourceId)
        } else 0
    }

    fun removeViewFromCurrentContainer(view: View) {
        if (systemOverlay) {
            overlayContainer!!.removeView(view)
        } else {
            (getActivityContentView() as ViewGroup).removeView(view)
        }
    }

    /**
     * Retrieves the screen size from the Activity context
     * @return the screen size as a Point object
     */
    private fun getScreenSize(): Point {
        val size = Point()
        getWindowManager().defaultDisplay.getSize(size)
        return size
    }

    inner class ActionViewClickListener : View.OnClickListener {

        override fun onClick(v: View) {
            toggle(animated)
        }
    }

    private inner class ItemViewQueueListener(private val item: Item) : Runnable {

        private val MAX_TRIES = 10
        private val tries: Int = 0

        override fun run() {

            if (item.view.measuredWidth == 0 && tries < MAX_TRIES) {
                item.view.post(this)
                return
            }
            // Measure the size of the item view
            item.width = item.view.measuredWidth
            item.height = item.view.measuredHeight

            // Revert everything back to normal
            item.view.alpha = item.alpha
            // Remove the item view from view hierarchy
            removeViewFromCurrentContainer(item.view)
        }

    }


    class Item(val view: View, var width: Int, var height: Int) {
        var x: Int = 0
        var y: Int = 0
        val alpha: Float = view.alpha
    }

    interface MenuStateChangeListener {
        fun onMenuOpened(menu: FloatingActionMenu)
        fun onMenuClosed(menu: FloatingActionMenu)
    }

    /**
     * A builder for {@link FloatingActionMenu} in conventional Java Builder format
     */
    class Builder(context: Context,  var systemOverlay: Boolean = false) {

        //in android 0 - right 90 -bottom 180 - left 270 - top WTF??????????
        var startAngle: Int = 180
        var endAngle: Int = 270
        private var radius: Int = context.resources.getDimensionPixelSize(R.dimen.action_menu_radius)
        lateinit var attachView: View
        private var subActionItems: MutableList<Item> = ArrayList<Item>()
        var animationHandler: MenuAnimationHandler = DefaultAnimationHandler()
        var animated: Boolean = true
        var stateChangeListener: MenuStateChangeListener? = null
        var clickListener: OnClickListener? = null

        fun addSubActionView(subActionView: View, width: Int = 0, height: Int = 0): Builder {
            subActionItems.add(Item(subActionView, width, height))
            return this
        }

        fun addSubActionViews(subActionViews: MutableList<View>, width: Int = 0, height: Int = 0): Builder {
            for (i in 0 until subActionViews.size)
                subActionItems.add(Item(subActionViews[i], width, height))

            return this
        }

        fun addSubActionView(resId: Int, context: Context): Builder {
            val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val view = inflater.inflate(resId, null, false)
            view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
            return this.addSubActionView(view, view.measuredWidth, view.measuredHeight)
        }

        fun build(): FloatingActionMenu {
            return FloatingActionMenu(attachView,
                    startAngle,
                    endAngle,
                    radius,
                    subActionItems,
                    animationHandler,
                    animated,
                    stateChangeListener,
                    clickListener,
                    systemOverlay)
        }
    }

    private fun getDefaultSystemWindowParams(): WindowManager.LayoutParams {
        val params = WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O)
                    WindowManager.LayoutParams.TYPE_PHONE else
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL or WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT)
        params.format = PixelFormat.RGBA_8888
        params.gravity = (Gravity.TOP or Gravity.LEFT)
        return params
    }

    init {
        this.animationHandler = animationHandler
        this.animated = animated
        this.systemOverlay = systemOverlay
        this.open = false
        this.stateChangeListener = stateChangeListener
        this.mainActionView.isClickable = true
        this.mainActionView.setOnClickListener(ActionViewClickListener())

        animationHandler.menu = this

        overlayContainer = if (systemOverlay) FrameLayout(mainActionView.context)
        else null // beware NullPointerExceptions!

        for (item in subActionItems) {
            if (item.width == 0 || item.height == 0) {
                if (systemOverlay) {
                    throw  RuntimeException("Sub action views cannot be added without " +
                            "definite width and height.")
                }
                // Figure out the size by temporarily adding it to the Activity content view hierarchy
                // and ask the size from the system
                addViewToCurrentContainer(item.view)
                // Make item view invisible, just in case
                item.view.alpha = 0f
                // Wait for the right time
                item.view.post(ItemViewQueueListener(item))
            }
        }
        if (systemOverlay) {
            orientationListener = object : OrientationEventListener(mainActionView.context, SensorManager.SENSOR_DELAY_UI) {
                var lastState = -1

                override fun onOrientationChanged(orientation: Int) {

                    val display = getWindowManager().defaultDisplay
                    if (display.rotation != lastState) {
                        lastState = display.rotation
                        if (open) {
                            close(false)
                        }
                    }
                }
            }
            orientationListener.enable()
        }
    }

    interface OnClickListener{
        fun onClick(position: Int)
    }

}