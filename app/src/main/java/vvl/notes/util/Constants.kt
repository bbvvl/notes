package vvl.notes.util

val DEBUG = true
val packageName = "vvl.notes"
val GAP = 100L
val DEFAULT_Z_VALUE = 25f
val DEFAULT_ANIMATION_DURATION = 200L
val PERMISSIONS_REQUEST_USE_FINGERPRINT = 0