package vvl.notes.db

import io.realm.Realm
import io.realm.RealmObject
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey
import io.realm.kotlin.where
import vvl.notes.util.GAP
import java.util.*


open class Note(@PrimaryKey var id: Long = 0, var note: String = "", var color: Int = -1, var time: Date = Date(), @Index var position: Long = 1) : RealmObject() {

    companion object {
        fun create(realm: Realm) {
            val id: Long = (realm.where<Note>().max("id"))?.toLong()?.inc() ?: 0
            val pos: Long = (realm.where<Note>().max("position"))?.toLong()?.plus(GAP) ?: 0
            realm.insert(Note(id = id, note = "", color = Random().nextInt(15), time = Date(), position = pos))
        }

        fun delete(realm: Realm, id: Long) =
                realm.where(Note::class.java).equalTo("id", id).findFirst()?.deleteFromRealm()


    }
}