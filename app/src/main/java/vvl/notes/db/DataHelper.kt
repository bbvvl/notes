package vvl.notes.db

import io.realm.Realm
import io.realm.Sort
import io.realm.kotlin.where
import vvl.notes.util.GAP


object DataHelper {
    fun deleteNoteAsync(realm: Realm, id: Long) {
        realm.executeTransactionAsync { realm -> Note.delete(realm, id) }
    }

    fun addNote(realm: Realm) {
        realm.executeTransaction { realm ->
            Note.create(realm)
        }
    }

    fun setNoteAsync(realm: Realm, changed: String, changedId: Long) {
        realm.executeTransaction({ realm ->
            realm.where<Note>().equalTo("id", changedId).findFirst()!!.note = changed
        })
    }

    fun move(realm: Realm, fromId: Long, toId: Long) {
        realm.executeTransaction({ realm ->
            val from = realm.where<Note>().equalTo("id", fromId).findFirst()
            val to = realm.where<Note>().equalTo("id", toId).findFirst()
            if (from == null || to == null) throw IllegalArgumentException("Neither item may be null.")

            when {
                (from.position < to.position) -> {
                    val afterTarget = realm.where<Note>()
                            .greaterThan("position", to.position)
                            .sort("position").findAll()
                    moveItemToBetween(realm, from, to, afterTarget.first(null))
                }
                (from.position > to.position) -> {
                    val beforeTarget = realm.where<Note>()
                            .lessThan("position", to.position).sort("position", Sort.DESCENDING)
                            .findAll()
                    moveItemToBetween(realm, from, beforeTarget.first(null), to)
                }
            }
        })
    }

    private fun moveItemToBetween(realm: Realm, itemToMove: Note, item1: Note?, item2: Note?) {
        if (item1 != null && itemToMove == item1 || item2 != null && itemToMove == item2) return

        var newPos = findMiddlePos(realm, item1, item2)
        if (newPos == null) {
            resetPositions(realm)
            newPos = findMiddlePos(realm, item1, item2)
            if (newPos == null)
                throw IllegalArgumentException("Couldn't find space between item1 and item2 after re-spacing")
        }

        itemToMove.position = newPos
    }

    private fun resetPositions(realm: Realm) {
        val orderedItems = ArrayList(realm.where<Note>().sort("position").findAll())
        var nextPos = 0L
        for (item in orderedItems) {
            item.position = nextPos
            nextPos += GAP
        }
    }

    private fun findMiddlePos(realm: Realm, item1: Note?, item2: Note?): Long? {

        if (item1 == null && item2 == null) throw IllegalArgumentException("Both items are null.")

        if (item1 == null) return item2!!.position - GAP
        if (item2 == null) return item1.position + GAP


        val p1 = item1.position
        val p2 = item2.position
        if (p2 <= p1) throw IllegalArgumentException("item2 was before or at the same position as item1.")
        val pos = (p1 + p2) / 2L
        return if (realm.where<Note>().equalTo("position", pos).findFirst() == null) pos else null
    }

    enum class SortType { Time, Note, Position }
}