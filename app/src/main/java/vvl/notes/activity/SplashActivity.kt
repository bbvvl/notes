package vvl.notes.activity

import android.content.Context
import android.hardware.fingerprint.FingerprintManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import vvl.notes.R
import vvl.notes.fingerprint.FingerprintUiHelper


class SplashActivity : AppCompatActivity(), FingerprintUiHelper.Callback {

    private val run = Handler()
    private var fingerprintEnabled = false

    private var mFingerprintUiHelper: FingerprintUiHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        val mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)

        fingerprintEnabled = mSharedPreferences.getBoolean(getString(R.string.use_fingerprint_to_authenticate_key), false)
        if (fingerprintEnabled && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) initFingerprint()
        else startAppWithDelay()


    }


    @RequiresApi(Build.VERSION_CODES.M)
    private fun initFingerprint() {

        val mFingerprintManager = getSystemService(Context.FINGERPRINT_SERVICE) as FingerprintManager
        if (!mFingerprintManager.isHardwareDetected) return
        setContentView(R.layout.activity_splash_fingerprint)
        val mFingerprintImageView = findViewById<ImageView>(R.id.activity_splash_fingerprint_text_view_fingerprint_imageView)
        val mFingerprintTextView = findViewById<TextView>(R.id.activity_splash_fingerprint_text_view_fingerprint_instructions)
        mFingerprintUiHelper = FingerprintUiHelper.FingerprintUiHelperBuilder(mFingerprintManager).build(mFingerprintImageView, mFingerprintTextView, this)
        try {
            if (mFingerprintUiHelper!!.isFingerprintAuthAvailable()) {
                mFingerprintImageView.visibility = View.VISIBLE
                mFingerprintTextView.visibility = View.VISIBLE
                mFingerprintUiHelper!!.startListening()
            } else {
                mFingerprintImageView.visibility = View.GONE
                mFingerprintTextView.visibility = View.GONE
            }
        } catch (e: SecurityException) {
            e.printStackTrace()
        }
    }

    private fun startAppWithDelay() {
        println("startAppWithDelay")
        setContentView(R.layout.activity_splash)
        run.postDelayed({
            startApp()
        }, 1000)
    }

    private fun startApp() {
        val intent = android.content.Intent(this@SplashActivity, vvl.notes.activity.MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onBackPressed() {
        super.onBackPressed()

        run.removeCallbacksAndMessages(null)
    }

    override fun onPause() {
        super.onPause()

        mFingerprintUiHelper?.stopListening()

    }

    override fun onAuthenticated() {
        startApp()
    }

    override fun onError() {
        Toast.makeText(this, "qwe", Toast.LENGTH_SHORT).show()
    }
}