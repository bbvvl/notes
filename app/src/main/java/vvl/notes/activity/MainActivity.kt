package vvl.notes.activity

import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.*
import io.realm.Realm
import io.realm.kotlin.where
import vvl.notes.R
import vvl.notes.db.DataHelper
import vvl.notes.db.Note
import vvl.notes.fab.FloatingActionMenu
import vvl.notes.recyclerView.MyAdapter
import vvl.notes.recyclerView.MyCallback
import vvl.notes.recyclerView.MyItemTouchHelperCallback


class MainActivity : AppCompatActivity(), MyCallback {
    private lateinit var adapter: MyAdapter

    private val realm: Realm = Realm.getDefaultInstance()
    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        adapter = MyAdapter(this, realm.where<Note>().sort("position").findAll())



        recyclerView = findViewById(R.id.activity_main_recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
        /*recyclerView.setOnTouchListener(object : View.OnTouchListener {
            //https://gist.github.com/kimcy929/3110c1c97aa20c5bc45c91ec29397699 maybe i can fix it(no)
            var handler = Handler()

            var numberOfTaps = 0
            var lastTapTimeMs: Long = 0
            var touchDownMs: Long = 0

            override fun onTouch(v: View?, event: MotionEvent?): Boolean {

                when (event!!.action) {
                    MotionEvent.ACTION_DOWN -> touchDownMs = System.currentTimeMillis()
                    MotionEvent.ACTION_UP -> {
                        handler.removeCallbacksAndMessages(null)

                        if (System.currentTimeMillis() - touchDownMs > ViewConfiguration.getTapTimeout()) {
                            //it was not a tap
                            numberOfTaps = 0
                            lastTapTimeMs = 0
                        }

                        numberOfTaps = if (numberOfTaps > 0 && System.currentTimeMillis() - lastTapTimeMs < ViewConfiguration.getDoubleTapTimeout())
                            numberOfTaps + 1 else 1

                        lastTapTimeMs = System.currentTimeMillis()

                        if (numberOfTaps === 2) {
                            handler.postDelayed(Runnable {
                                //handle double tap
                                DataHelper.addNote(realm)
                                adapter.notifyItemInserted(adapter.itemCount)
                                recyclerView.smoothScrollToPosition(adapter.itemCount)

                            }, ViewConfiguration.getTapTimeout().toLong())
                        }
                    }
                }

                return false
            }
        })*/
        ItemTouchHelper(MyItemTouchHelperCallback(adapter)).attachToRecyclerView(recyclerView)

        initFloatingActionMenu()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_activity_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    //TODO fix this shit
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.sort_by_name -> {
                adapter.swap(realm, DataHelper.SortType.Note)
                true
            }
            R.id.sort_by_rating -> {
                adapter.swap(realm, DataHelper.SortType.Position)
                true
            }
            R.id.sort_by_birth -> {
                adapter.swap(realm, DataHelper.SortType.Time)
                true
            }

            R.id.main_activity_menu_settings -> {
                startActivity(Intent(this, SettingsActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        recyclerView.adapter = null
        realm.close()
    }

    override fun removeAt(position: Long) {
        DataHelper.deleteNoteAsync(realm, position)
    }

    override fun move(fromId: Long, toId: Long) {
        DataHelper.move(realm, fromId, toId)
    }

    override fun onTextChanged(changed: String, changedId: Long) {
        DataHelper.setNoteAsync(realm, changed, changedId)
    }

    private fun initFloatingActionMenu() {
        val backgroundColor = R.color.background5

        val actionButton = findViewById<FloatingActionButton>(R.id.activity_main_floating_action_button)
        actionButton.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(this, backgroundColor))

        FloatingActionMenu.Builder(this).apply {
            addSubActionViews(arrayListOf(
                    button(resId = R.drawable.main_activity_floating_action_menu_item_text, backgroundColor = backgroundColor),
                    button(resId = R.drawable.main_activity_floating_action_menu_item_image, backgroundColor = backgroundColor),
                    button(resId = R.drawable.main_activity_floating_action_menu_item_sound, backgroundColor = backgroundColor),
                    button(resId = R.drawable.main_activity_floating_action_menu_item_custom, backgroundColor = backgroundColor)),
                    resources.getDimensionPixelSize(android.support.design.R.dimen.design_fab_size_mini), resources.getDimensionPixelSize(android.support.design.R.dimen.design_fab_size_mini))
            clickListener = object : FloatingActionMenu.OnClickListener {
                override fun onClick(position: Int) {
                    when (position) {
                        0 -> {
                            DataHelper.addNote(realm)
                            adapter.notifyItemInserted(adapter.itemCount)
                            recyclerView.smoothScrollToPosition(adapter.itemCount)

                            println("0")
                        }
                        1 -> {
                            println("1")
                        }
                        2 -> {
                            println("2")
                        }
                        3 -> {
                            println("3")
                        }
                        else -> {
                            println("error")
                        }
                    }
                }
            }
            attachView = actionButton
            build()
        }

    }

    private fun button(resId: Int, context: Context = this, tint: Int = Color.WHITE, sizeBtn: Int = FloatingActionButton.SIZE_MINI, rippleColorBtn: Int = R.color.my_ripple_material_light, backgroundColor: Int = R.color.colorAccent): FloatingActionButton = FloatingActionButton(context).apply {
        size = sizeBtn
        setImageResource(resId)
        setColorFilter(tint)
        rippleColor = resources.getColor(rippleColorBtn)
        backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, backgroundColor))
    }
}
