/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */
package vvl.notes.fingerprint


import android.annotation.TargetApi
import android.app.KeyguardManager
import android.content.Context
import android.hardware.fingerprint.FingerprintManager
import android.os.Build
import android.os.CancellationSignal
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.widget.ImageView
import android.widget.TextView
import vvl.notes.R
import java.security.InvalidAlgorithmParameterException
import java.security.KeyStore
import java.security.NoSuchAlgorithmException
import java.security.NoSuchProviderException
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
//TODO permissions
@TargetApi(Build.VERSION_CODES.M)
/**
 * Constructor for {@link FingerprintUiHelper}. This method is expected to be called from
 * only the {@link FingerprintUiHelperBuilder} class.
 */
class FingerprintUiHelper(fingerprintManager: FingerprintManager, icon: ImageView, errorTextView: TextView, callback: Callback) : FingerprintManager.AuthenticationCallback() {

    /**
     * The timeout for the error to be displayed. Returns to the normal UI after this.
     */
    private val ERROR_TIMEOUT_MILLIS = 1300L
    /**
     * The timeout for the success to be displayed. Calls {@link Callback#onAuthenticated()} after this.
     */
    private val SUCCESS_DELAY_MILLIS = 1000L
    /**
     * Alias for our key in the Android Key Store
     **/
    val KEY_NAME = "vvl.notes.util.mykey"

    /**
     * The {@link Cipher} used to init {@link FingerprintManager}
     */
    private lateinit var mCipher: Cipher
    /**
     * The {@link KeyStore} used to initiliaze the key {@link #KEY_NAME}
     */
    private var mKeyStore: KeyStore? = null
    /**
     * The {@link KeyGenerator} used to generate the key {@link #KEY_NAME}
     */
    private lateinit var mKeyGenerator: KeyGenerator
    /**
     * The {@link android.hardware.fingerprint.FingerprintManager.CryptoObject}
     */
    private val mFingerprintManager: FingerprintManager = fingerprintManager
    /**
     * The {@link ImageView} that is used to show the authent state
     */
    private val mIcon: ImageView = icon
    /**
     * The {@link TextView} that is used to show the authent state
     */
    private val mErrorTextView: TextView = errorTextView
    /**
     * The {@link com.github.omadahealth.lollipin.lib.managers.FingerprintUiHelper.Callback} used to return success or error.
     */
    private val mCallback: Callback = callback
    /**
     * The {@link CancellationSignal} used after an error happens
     */
    private var mCancellationSignal: CancellationSignal? = null
    /**
     * Used if the user cancelled the authentication by himself
     */
    private var mSelfCancelled: Boolean = true

    /**
     * Builder class for {@link FingerprintUiHelper} in which injected fields from Dagger
     * holds its fields and takes other arguments in the {@link #build} method.
     */

    class FingerprintUiHelperBuilder(fingerprintManager: FingerprintManager) {
        private val mFingerPrintManager: FingerprintManager = fingerprintManager

        fun build(icon: ImageView, errorTextView: TextView, callback: Callback): FingerprintUiHelper {
            return FingerprintUiHelper(mFingerPrintManager, icon, errorTextView,
                    callback)
        }
    }

    /**
     * Starts listening to {@link FingerprintManager}
     *
     * @throws SecurityException If the hardware is not available, or the permission are not set
     */
    fun startListening() {
        if (initCipher()) {
            val cryptoObject = FingerprintManager.CryptoObject(mCipher)
            if (!isFingerprintAuthAvailable()) {
                return
            }
            mCancellationSignal = CancellationSignal()
            mSelfCancelled = false
            mFingerprintManager.authenticate(cryptoObject, mCancellationSignal, 0 /* flags */, this, null)
            mIcon.setImageResource(R.drawable.ic_fp_40px)
        }
    }

    /**
     * Stops listening to {@link FingerprintManager}
     */
    fun stopListening() {
        if (mCancellationSignal != null) {
            mSelfCancelled = true
            mCancellationSignal!!.cancel()
            mCancellationSignal = null
        }
    }

    /**
     * Called by {@link FingerprintManager} if the authentication threw an error.
     */
    override fun onAuthenticationError(errMsgId: Int, errString: CharSequence) {
        if (!mSelfCancelled) {
            showError(errString)
            mIcon.postDelayed({ mCallback.onError() }, ERROR_TIMEOUT_MILLIS)
        }
    }

    /**
     * Called by {@link FingerprintManager} if the user asked for help.
     */
    override fun onAuthenticationHelp(helpMsgId: Int, helpString: CharSequence) {
        showError(helpString)
    }

    /**
     * Called by {@link FingerprintManager} if the authentication failed (bad finger etc...).
     */
    override fun onAuthenticationFailed() {
        showError(mIcon.resources.getString(R.string.pin_code_fingerprint_not_recognized))
    }

    /**
     * Called by {@link FingerprintManager} if the authentication succeeded.
     */
    override fun onAuthenticationSucceeded(result: FingerprintManager.AuthenticationResult) {
        mErrorTextView.removeCallbacks(mResetErrorTextRunnable)
        mIcon.setImageResource(R.drawable.ic_fingerprint_success)
        mErrorTextView.setTextColor(
                mErrorTextView.resources.getColor(R.color.success_color, null))
        mErrorTextView.text = mErrorTextView.resources.getString(R.string.pin_code_fingerprint_success)
        mIcon.postDelayed({ mCallback.onAuthenticated() }, SUCCESS_DELAY_MILLIS)
    }

    /**
     * Tells if the {@link FingerprintManager#isHardwareDetected()}, {@link FingerprintManager#hasEnrolledFingerprints()},
     * and {@link KeyguardManager#isDeviceSecure()}
     *
     * @return true if yes, false otherwise
     * @throws SecurityException If the hardware is not available, or the permission are not set
     */
    fun isFingerprintAuthAvailable() =
            mFingerprintManager.isHardwareDetected &&
                    mFingerprintManager.hasEnrolledFingerprints() &&
                    (mIcon.context.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager).isDeviceSecure


    /**
     * Initialize the {@link Cipher} instance with the created key in the {@link #createKey()}
     * method.
     *
     * @return {@code true} if initialization is successful, {@code false} if the lock screen has
     * been disabled or reset after the key was generated, or if a fingerprint got enrolled after
     * the key was generated.
     */
    private fun initCipher(): Boolean =
            try {
                if (mKeyStore == null) {
                    mKeyStore = KeyStore.getInstance("AndroidKeyStore")
                }
                createKey()
                mKeyStore!!.load(null)
                val key = mKeyStore!!.getKey(KEY_NAME, null)
                mCipher = Cipher.getInstance("AES/CBC/PKCS7Padding")
                mCipher.init(Cipher.ENCRYPT_MODE, key)
                true
            } catch (e: Exception) {
                false
            }


    /**
     * Creates a symmetric key in the Android Key Store which can only be used after the user has
     * authenticated with fingerprint.
     */
    private fun createKey() {
        // The enrolling flow for fingerprint. This is where you ask the user to set up fingerprint
        // for your flow. Use of keys is necessary if you need to know if the set of
        // enrolled fingerprints has changed.
        try {
            // Set the alias of the entry in Android KeyStore where the key will appear
            // and the constrains (purposes) in the constructor of the Builder
            mKeyGenerator = KeyGenerator.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore")
            mKeyGenerator.init(KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    // Require the user to authenticate with a fingerprint to authorize every use
                    // of the key
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build())
            mKeyGenerator.generateKey()
        } catch (e: Exception) {

            when (e) {
                is NoSuchProviderException,
                is NoSuchAlgorithmException,
                is InvalidAlgorithmParameterException -> throw  RuntimeException(e)
            }
        }
    }

    /**
     * Show an error on the UI using {@link #mIcon} and {@link #mErrorTextView}
     */
    private fun showError(error: CharSequence) {
        mIcon.setImageResource(R.drawable.ic_fingerprint_error)
        mErrorTextView.text = error
        mErrorTextView.setTextColor(
                mErrorTextView.resources.getColor(R.color.warning_color, null))
        mErrorTextView.removeCallbacks(mResetErrorTextRunnable)
        mErrorTextView.postDelayed(mResetErrorTextRunnable, ERROR_TIMEOUT_MILLIS)
    }

    /**
     * Run by {@link #showError(CharSequence)} with delay to reset the original UI after an error.
     */
    private val mResetErrorTextRunnable = Runnable {
        mErrorTextView.setTextColor(mErrorTextView.resources.getColor(R.color.hint_color, null))
        mErrorTextView.text = mErrorTextView.resources.getString(R.string.fingerprint_text)
        mIcon.setImageResource(R.drawable.ic_fp_40px)
    }

    /**
     * The interface used to call the original Activity/Fragment... that uses this helper.
     */
    interface Callback {
        fun onAuthenticated()

        fun onError()
    }
}